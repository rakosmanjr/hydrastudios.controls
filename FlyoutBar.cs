﻿// Copyright (C) HydraStudios
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace HydraStudios.Controls
{
	public class FlyoutBar : ListView
	{
		public const String PART_FlyoutBarButton = nameof(PART_FlyoutBarButton);

		public static readonly DependencyProperty ContentProperty = DependencyProperty.Register(nameof(Content), typeof(Object), typeof(FlyoutBar), new FrameworkPropertyMetadata(null));
		public static readonly DependencyProperty ContentIndexProperty = DependencyProperty.Register(nameof(ContentIndex), typeof(Int32), typeof(FlyoutBar), new FrameworkPropertyMetadata(WhenContentIndexPropertyChanged));
		public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register(nameof(IsOpen), typeof(Boolean), typeof(FlyoutBar), new FrameworkPropertyMetadata(false));
		public static readonly DependencyProperty FlyoutPanelWidthProperty = DependencyProperty.Register(nameof(FlyoutPanelWidth), typeof(Double), typeof(FlyoutBar), new FrameworkPropertyMetadata(250.0, (FrameworkPropertyMetadataOptions)0b1111));

		private readonly Dictionary<Button, Boolean> buttonStates;
		private readonly Dictionary<Button, FlyoutBarItem> buttonItems;

		/// <summary>
		/// Current opened content.
		/// </summary>
		public Object Content
		{
			get => (ContentControl)GetValue(ContentProperty);
			set => SetValue(ContentProperty, value);
		}

		/// <summary>
		/// Index in Items for the current opened item.
		/// </summary>
		public Int32 ContentIndex
		{
			get => (Int32)GetValue(ContentIndexProperty);
			set => SetValue(ContentIndexProperty, value);
		}

		/// <summary>
		/// True if the flyout panel is open.
		/// </summary>
		public Boolean IsOpen
		{
			get => (Boolean)GetValue(IsOpenProperty);
			set => SetValue(IsOpenProperty, value);
		}

		/// <summary>
		/// The width of the flyout panel.
		/// </summary>
		public Double FlyoutPanelWidth
		{
			get => (Double)GetValue(FlyoutPanelWidthProperty);
			set => SetValue(FlyoutPanelWidthProperty, value);
		}

		static FlyoutBar()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(FlyoutBar), new FrameworkPropertyMetadata(typeof(FlyoutBar)));
		}

		public FlyoutBar()
		{
			buttonStates = new Dictionary<Button, Boolean>();
			buttonItems = new Dictionary<Button, FlyoutBarItem>();

			ItemContainerGenerator.StatusChanged += (s, e) =>
			{
				if (ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
				{
					foreach (var item in Items.OfType<FlyoutBarItem>())
					{
						var container = ItemContainerGenerator.ContainerFromItem(item);
						var containerButton = GetChildrenOfType<Button>(container).FirstOrDefault(b => Equals(b.Name, PART_FlyoutBarButton));

						if (containerButton != null)
						{
							buttonItems.Add(containerButton, item);
							buttonStates.Add(containerButton, false);

							containerButton.Click += WhenFlyoutBarButtonClicked;
						}
					}

					WhenContentIndexPropertyChanged(this, new DependencyPropertyChangedEventArgs());
				}
				else if (ItemContainerGenerator.Status == GeneratorStatus.GeneratingContainers)
				{
					foreach (var button in buttonStates.Keys)
					{
						button.Click -= WhenFlyoutBarButtonClicked;
					}

					buttonItems.Clear();
					buttonStates.Clear();
				}
			};
		}
		
		private static void WhenContentIndexPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (d is FlyoutBar flyoutBar)
			{
				if (flyoutBar.Items.Count > flyoutBar.ContentIndex && flyoutBar.ContentIndex >= 0)
				{
					flyoutBar.Content = ((FlyoutBarItem)flyoutBar.Items[flyoutBar.ContentIndex])?.Content;
				}
			}
		}

		private static IEnumerable<T> GetChildrenOfType<T>(DependencyObject depObj)
			where T : DependencyObject
		{
			if (depObj == null)
			{
				yield break;
			}

			for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); ++i)
			{
				var child = VisualTreeHelper.GetChild(depObj, i);

				if (child is T validChild)
				{
					yield return validChild;
				}

				foreach (var foundChild in GetChildrenOfType<T>(child))
				{
					yield return foundChild;
				}
			}
		}

		private void WhenFlyoutBarButtonClicked(Object sender, RoutedEventArgs e)
		{
			if (sender is Button button)
			{
				if (buttonStates.TryGetValue(button, out var state))
				{
					if (state)
					{
						buttonStates[button] = false;
						IsOpen = false;
					}
					else
					{
						var buttons = buttonStates.Keys.ToList();

						foreach (var b in buttons)
						{
							buttonStates[b] = false;
						}

						buttonStates[button] = true;

						Content = buttonItems[button].Content;
						IsOpen = true;
					}
				}
			}
		}
	}
}
